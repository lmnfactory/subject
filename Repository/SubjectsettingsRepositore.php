<?php

namespace Lmn\Subject\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Subject\Database\Model\Subjectsettings;

class SubjectsettingsRepository extends AbstractEloquentRepository {

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
    }

    public function getModel() {
        return Subjectsettings::class;
    }

    public function update($data) {
        $subjectsettings = parent::update($data);
        $subjectsettings = $this->clear()
            ->criteria('core.id', ['id' => $subjectsettings->id])
            ->criteria('subjectsettings.with.all')
            ->get();
        return $subjectsettings;
    }
}

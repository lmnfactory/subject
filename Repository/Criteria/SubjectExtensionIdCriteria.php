<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectExtensionIdCriteria implements Criteria {

    private $subjectId;

    public function __construct() {

    }

    public function set($args) {
        $this->subjectId = $args['subjectId'];
    }

    public function apply(Builder $builder) {
        $builder->where('subject_id', '=', $this->subjectId);
    }
}

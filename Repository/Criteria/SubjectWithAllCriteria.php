<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectWithAllCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {

    }

    public function apply(Builder $builder) {
        $builder->with(['subjectsettings', 'subjectsettings.universitypersons', 'subjectsettings.universitypersons.person', 'subjectprototype', 'subjectprototype.faculty']);
    }
}

<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

class SubjectByUserCriteria implements Criteria {

    private $userId;

    public function __construct() {

    }

    public function set($data) {
        $this->userId = $data['userId'];
    }

    public function apply(Builder $builder) {
        $self = $this;
        $builder->join('subject_user', 'subject_user.subject_id', '=', 'subject.id')
            ->with(['usersettings' => function($query) use ($self) {
                $query->where('subject_user.user_id', '=', $self->userId);
            }])
            ->where('subject_user.user_id', '=', $this->userId);
    }
}

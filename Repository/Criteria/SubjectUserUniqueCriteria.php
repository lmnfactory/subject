<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

class SubjectUserUniqueCriteria implements Criteria {

    private $userId;
    private $subjectId;

    public function __construct() {

    }

    public function set($data) {
        $this->userId = $data['userId'];
        $this->subjectId = $data['subjectId'];
    }

    public function apply(Builder $builder) {
        $builder->where('subject_user.user_id', '=', $this->userId)
            ->where('subject_user.subject_id', '=', $this->subjectId);
    }
}

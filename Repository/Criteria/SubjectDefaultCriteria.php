<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectDefaultCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {

    }

    public function apply(Builder $builder) {
        $builder->select(['subject.*'])
            ->join('subjectprototype', 'subjectprototype.id', '=', 'subject.subjectprototype_id')
            ->where('subject.active', '=', true)
            ->orderBy('subjectprototype.name', 'asc');
    }
}

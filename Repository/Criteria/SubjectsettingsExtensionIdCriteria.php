<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectsettingsExtensionIdCriteria implements Criteria {

    private $subjectsettingsId;

    public function __construct() {

    }

    public function set($args) {
        $this->subjectsettingsId = $args['subjectsettingsId'];
    }

    public function apply(Builder $builder) {
        $builder->where('subjectsettings_id', '=', $this->subjectsettingsId);
    }
}

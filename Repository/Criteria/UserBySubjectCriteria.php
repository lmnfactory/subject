<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

class UserBySubjectCriteria implements Criteria {

    private $subjectId;

    public function __construct() {

    }

    public function set($data) {
        $this->subjectId = $data['subjectId'];
    }

    public function apply(Builder $builder) {
        $builder->where('subject_user.subject_id', '=', $this->subjectId);
    }
}

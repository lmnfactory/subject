<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

class SubjectByPublicIdCriteria implements Criteria {

    private $publicId;
    private $criteriaService;

    public function __construct(CriteriaService $criteriaService) {
        $this->criteriaService = $criteriaService;
    }

    public function set($data) {
        $this->publicId = $data['publicId'];
    }

    public function apply(Builder $builder) {
        $builder->where('subjectprototype.public_id', '=', $this->publicId);
    }
}

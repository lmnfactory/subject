<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectuserExtensionIdCriteria implements Criteria {

    private $subjectuserId;

    public function __construct() {

    }

    public function set($args) {
        $this->subjectuserId = $args['subjectuserId'];
    }

    public function apply(Builder $builder) {
        $builder->where('subject_user_id', '=', $this->subjectuserId);
    }
}

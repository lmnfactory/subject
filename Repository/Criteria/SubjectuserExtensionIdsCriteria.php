<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectuserExtensionIdsCriteria implements Criteria {

    private $subjectuserIds;

    public function __construct() {

    }

    public function set($args) {
        $this->subjectuserIds = $args['subjectuserIds'];
    }

    public function apply(Builder $builder) {
        $builder->whereIn('subject_user_id', $this->subjectuserIds);
    }
}

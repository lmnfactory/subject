<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectExtensionIdsCriteria implements Criteria {

    private $subjectIds;

    public function __construct() {

    }

    public function set($args) {
        $this->subjectIds = $args['subjectIds'];
    }

    public function apply(Builder $builder) {
        $builder->whereIn('subject_id', $this->subjectIds);
    }
}

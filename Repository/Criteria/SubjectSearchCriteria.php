<?php

namespace Lmn\Subject\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SubjectSearchCriteria implements Criteria {

    private $search;

    public function __construct() {

    }

    public function set($data) {
        $this->search = $data['search'];
    }

    public function apply(Builder $builder) {
        $self = $this;
        $builder->where(function($query) use ($self){
            $searchArray = explode(" ", $self->search);

            $query->where('subjectprototype.code', 'like', '%'.$self->search.'%');

            foreach ($searchArray as $s) {
                $query->orWhere('subjectprototype.name', 'like', '%'.$s.'%');
            }   
        });
    }
}

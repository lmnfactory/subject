<?php

namespace Lmn\Subject\Repository;

use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Subject\Database\Model\Subject_user;

class SubjectUserForUserRepository extends AbstractEventEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Subject_user::class;
    }
}

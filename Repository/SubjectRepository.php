<?php

namespace Lmn\Subject\Repository;
use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Subject\Database\Model\Subject;
use Lmn\Subject\Database\Model\Subjectsettings;
use Lmn\Subject\Database\Model\Subjectprototype;

class SubjectRepository extends AbstractEventEloquentRepository {

    private $generatorervice;
    private $saveService;

    public function __construct(CriteriaService $criteriaService, GeneratorService $generatorervice, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->generatorervice = $generatorervice;
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Subject::class;
    }

    public function get() {
        $subject = parent::get();

        // $extensions = $this->config->get('extensions');
        // if (is_array($extensions)) {
        //     foreach ($extensions as $key => $ext) {
        //         $subject->$key = $ext->clear()
        //             ->criteria('subject.ext.id', ['subjectId' => $subject->id])
        //             ->get();
        //     }
        // }

        return $subject;
    }

    public function all() {
        $subjects = parent::all();

        // $subjectIds = [];
        // foreach ($subjects as $val) {
        //     $subjectIds[] = $val->id;
        // }

        // $extensions = $this->config->get('extensions');
        // if (is_array($extensions)) {
        //     foreach ($extensions as $key => $ext) {
        //         $extData = $ext->clear()
        //             ->criteria('subject.ext.ids', ['subjectIds' => $subjectIds])
        //             ->all();

        //         foreach ($subjects as $skey => $val) {
        //             if (isset($extData[$val->id])) {
        //                 $subjects[$skey]->$key = $extData[$val->id];
        //             }
        //         }
        //     }
        // }

        return $subjects;
    }

    public function create($data) {
        $subjectprototype = new Subjectprototype();
        $subjectprototype->fill($data);
        $subjectprototype->public_id = $this->generatorervice->uniqueString('subjectprototype', 'public_id');

        $subjectsettings = new Subjectsettings();
        if (isset($data['subjectsettings'])) {
            $subjectsettings->fill($data['subjectsettings']);
        }

        $subject = new Subject();
        $subject->active = 1;

        $this->saveService->begin()
            ->has($subject, $subjectprototype)
            ->has($subjectsettings, $subject)
            ->transaction();

        $subject->subjectprototype;
        $subject->subjectsettings;

        return $subject;
    }
}

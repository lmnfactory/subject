<?php

namespace Lmn\Subject\Repository\Config;

use Lmn\Core\Lib\Repository\Config\AbstractRepositoryConfig;

class SubjectConfig extends AbstractRepositoryConfig {

    public function __construct() {
        parent::__construct();
    }
}

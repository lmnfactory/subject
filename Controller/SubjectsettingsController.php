<?php

namespace Lmn\Subject\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\University\Repository\UniversitypersonRepository;

use Lmn\Subject\Database\Model\Subject;
use Lmn\Subject\Lib\Subject\SubjectService;
use Lmn\Subject\Repository\SubjectRepository;
use Lmn\Subject\Repository\SubjectsettingsRepository;
use Lmn\Subject\Repository\SubjectsettingsPersonRepository;

class SubjectsettingsController extends Controller 
{
    public function getDetail(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectRepository $subjectRepo) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'subject.public_id')) {
            return $responseService->use('validation.system');
        }

        $subject = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subject.by.publicId', ['publicId' => $data['subject_pid']])
            ->get();

        if ($subject->subjectsettings) {
            $subject->subjectsettings->subjectprototype = $subject->subjectprototype;
        } else {
            $subject->subjectsettings = [
                'subjectprototype' => $subject->subjectprototype
            ];
        }

        return $responseService->response($subject->subjectsettings);
    }

    public function update(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectsettingsRepository $subjectSettingsRepo, CurrentUser $currentUser, SubjectRepository $subjectRepo, UniversitypersonRepository $universitypersonRepo, SubjectsettingsPersonRepository $subjectsettingsPersonRepo) {
        $data = $request->json()->all();

        $data['edited_by'] = $currentUser->getId();
        if (!$validationService->validate($data, 'form.subjectsettings.update')) {
            return $responseService->use('validation.data');
        }

        $subjectSettingsRepo->clear()
            ->criteria('core.id', ['id' => $data['id']])
            ->update($data);

        if (isset($data['persons'])) {
            $subjectsettingsPersonRepo->clear()
                ->criteria('subjectsettings.ext.id', ['subjectsettingsId' => $data['id']])
                ->delete();

            foreach ($data['persons'] as $person) {
                $universityperson = $universitypersonRepo->clear()
                    ->criteria('universityperson.by.name', ['name' => $person['name']])
                    ->get();
                
                if ($universityperson == null) {
                    $universityperson = $universitypersonRepo->clear()
                        ->create($person);
                }
                $person['universityperson_id'] = $universityperson->id;
                $person['subjectsettings_id'] = $data['id'];
                $subjectsettingsPersonRepo->clear()
                    ->create($person);
            }
            
        }

        $subject = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('core.id', ['id' => $data['subject_id'], 'table' => 'subject'])
            ->get();

        $subject->subjectsettings->subjectprototype = $subject->subjectprototype;

        return $responseService->response($subject->subjectsettings);
    }
}

<?php

namespace Lmn\Subject\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\Subject\Database\Model\Subject;
use Lmn\Subject\Lib\Subject\SubjectService;
use Lmn\Subject\Repository\SubjectRepository;

class SubjectController extends Controller {

    public function getDetail(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectRepository $subjectRepo) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'subject.public_id')) {
            return $responseService->use('validation.system');
        }

        $subject = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subject.by.publicId', ['publicId' => $data['subject_pid']])
            ->get();

        return $responseService->response($subject);
    }

    public function getList(Request $request, ResponseService $responseService, SubjectRepository $subjectRepo) {
        $data = $request->json()->all();

        $list = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->all();

        $response = $responseService->createMessage($list);
        $response->setOption([
            'totalItems' => sizeof($list)
        ]);

        return $responseService->send($response);
    }

    public function search(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectRepository $subjectRepo) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'subject.search')) {
            return $responseService->use('validation.system');
        }

        $list = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subject.search', ['search' => $data['search']])
            ->criteria('core.limit', ['limit' => 10])
            ->all();

        $response = $responseService->createMessage($list);
        $response->setOption([
            'totalItems' => sizeof($list)
        ]);

        return $responseService->send($response);
    }

    public function create(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectRepository $subjectRepo) {
        $data = $request->json()->all();

        if (!$validationService->validate($data, 'form.subject.create')) {
            return $responseService->use('validation.data');
        }

        $subject = $subjectRepo->clear()
            ->create($data);

        return $responseService->response($subject);
    }

    public function getPrototype(Request $request, ResponseService $responseService, ValidationService $validationService, SubjectRepository $subjectRepo) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'subject.id')) {
            return $responseService->use('validation.system');
        }

        $subject = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.prototype')
            ->criteria('core.id', ['id' => $data['id'], 'table' => 'subject'])
            ->get();

        return $responseService->response($subject);
    }

    public function update($publicId) {

    }

    public function archive($publicId) {

    }
}

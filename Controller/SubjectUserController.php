<?php

namespace Lmn\Subject\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Subject\Repository\SubjectRepository;
use Lmn\Subject\Repository\SubjectUserRepository;
use Lmn\Subject\Repository\SubjectUserForUserRepository;

class SubjectUserController extends Controller {

    public function getSubjects(Request $request, ResponseService $responseService, CurrentUser $currentUser, SubjectRepository $subjectRepo, SubjectUserForUserRepository $subjectUserRepo) {
        $data = $request->json()->all();

        $subjectUser = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subjectuser.by.user', ['userId' => $currentUser->getId()])
            ->all();

        return $responseService->response($subjectUser);
    }
}

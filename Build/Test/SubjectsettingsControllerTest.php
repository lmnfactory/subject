<?php

namespace Lmn\Subject\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubjectsettingsControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testDetail() {
        $this->authJson('POST', '/api/subject/settings', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => ['subject_id', 'edited_by', 'pointslimit', 'pointsdelimeter', 'webpage', 'description', 'universitypersons'],
            ]);
    }

    public function testDetailInvalidPublicId() {
        $this->authJson('POST', '/api/subject/settings', [
                'subject_pid' => 'ci3TReiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseStatus(400);
    }

    public function testDetailUnauthorise() {
        $this->json('POST', '/api/subject/settings', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseStatus(401);
    }
}

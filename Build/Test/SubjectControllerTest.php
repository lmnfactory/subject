<?php

namespace Lmn\Subject\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubjectControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testCreate() {
        $this->authJson('POST', '/api/subject/create', [
                'faculty_id' => '1',
                'name' => 'Test nazov',
                'code' => 'TN'
            ])
            ->assertResponseOk()
            ->seeJsonStructure(['data']);
    }

    public function testCreateUnauthorize() {
        $this->json('POST', '/api/subject/create', [
                'faculty_id' => '1',
                'name' => 'Test nazov',
                'code' => 'TN'
            ])
            ->assertResponseStatus(401);
    }

    public function testCreateMissingFaculty() {
        $this->authJson('POST', '/api/subject/create', [
                'name' => 'Test nazov',
                'code' => 'TN'
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateMissingName() {
        $this->authJson('POST', '/api/subject/create', [
                'faculty_id' => '1',
                'code' => 'TN'
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateMissingCode() {
        $this->authJson('POST', '/api/subject/create', [
                'faculty_id' => '1',
                'name' => 'Test nazov'
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateInvalidCode() {
        $this->authJson('POST', '/api/subject/create', [
                'faculty_id' => '1',
                'name' => 'Test nazov',
                'code' => 'TNTNTNTNTNT'
            ])
            ->assertResponseStatus(422);
    }

    public function testSearch() {
        $this->json('POST', '/api/subject/search', [
                'search' => 'Databaza'
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data',
                'option' => ['totalItems']
            ]);
    }

    public function testSearchMissingSearch() {
        $this->json('POST', '/api/subject/search', [])
            ->assertResponseStatus(400);
    }

    public function testDetail() {
        $this->authJson('POST', '/api/subject/detail', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseOk()
            ->seeJsonStructure(['data']);
    }

    public function testDetailUnauthorize() {
        $this->json('POST', '/api/subject/detail', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseStatus(401);
    }

    public function testDetailMissingSubjectPid() {
        $this->authJson('POST', '/api/subject/detail', [])
            ->assertResponseStatus(400);
    }

    public function testDetailInvalidSubjectPid() {
        $this->authJson('POST', '/api/subject/detail', [
                'subject_pid' => '3eiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseStatus(400);
    }
}

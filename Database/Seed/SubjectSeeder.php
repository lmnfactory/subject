<?php

namespace Lmn\Subject\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('subject')->insert([
            [
                'id' => 1,
                'subjectprototype_id' => 1,
                'active' => 1
            ],
            [
                'id' => 2,
                'subjectprototype_id' => 2,
                'active' => 1
            ],
            [
                'id' => 3,
                'subjectprototype_id' => 3,
                'active' => 1
            ]
        ]);
    }
}

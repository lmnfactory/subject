<?php

namespace Lmn\Subject\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class SubjectsettingsSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('subjectsettings')->insert([
            [
                'id' => 1,
                'subject_id' => 1
            ],
            [
                'id' => 2,
                'subject_id' => 2
            ],
            [
                'id' => 3,
                'subject_id' => 3
            ],
        ]);
    }
}

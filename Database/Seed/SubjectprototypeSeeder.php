<?php

namespace Lmn\Subject\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class SubjectprototypeSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('subjectprototype')->insert([
            [
                'id' => 1,
                'faculty_id' => 1,
                'name' => 'Procedurálne programovanie',
                'code' => 'PrPr',
                'public_id' => '3TciReiHTJi9pklppzfUM8dsIHonx81F'
            ],
            [
                'id' => 2,
                'faculty_id' => 1,
                'name' => 'Dátové štruktúry a algoritmy',
                'code' => 'DSA',
                'public_id' => 'M8dsIpklppz3TciReiHTJi9fUHonx81F'
            ],
            [
                'id' => 3,
                'faculty_id' => 1,
                'name' => 'Operačné systémy',
                'code' => 'OS',
                'public_id' => 'pklppz3TciReiHTJi9fUM8dsIHonx81F'
            ],
        ]);
    }
}

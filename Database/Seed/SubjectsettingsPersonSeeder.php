<?php

namespace Lmn\Subject\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class SubjectsettingsPersonSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('subjectsettings_person')->insert([
            [
                'subjectsettings_id' => 1,
                'universityperson_id' => 1,
                'universitypersontype_id' => 1
            ],
            [
                'subjectsettings_id' => 1,
                'universityperson_id' => 2,
                'universitypersontype_id' => 2
            ],
            [
                'subjectsettings_id' => 1,
                'universityperson_id' => 3,
                'universitypersontype_id' => 2
            ],
        ]);
    }
}

<?php

namespace Lmn\Subject\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class SubjectUserSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }

        \DB::table('subject_user')->insert([
            [
                'id' => 1,
                'subject_id' => 1,
                'user_id' => 1
            ],
            [
                'id' => 2,
                'subject_id' => 2,
                'user_id' => 1
            ],
            [
                'id' => 3,
                'subject_id' => 3,
                'user_id' => 1
            ],
            [
                'id' => 4,
                'subject_id' => 1,
                'user_id' => 2
            ]
        ]);
    }
}

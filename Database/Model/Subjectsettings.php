<?php

namespace Lmn\Subject\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Subject\Database\Model\SubjectsettingsPerson;

class Subjectsettings extends Model {

    protected $table = 'subjectsettings';

    protected $fillable = ['subject_id', 'edited_by', 'description', 'webpage', 'pointslimit', 'pointsdelimeter'];

    public function universitypersons() {
        return $this->hasMany(SubjectsettingsPerson::class);
    }
}

<?php

namespace Lmn\Subject\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model {

    protected $table = 'subject';

    protected $fillable = ['subjectprototype_id', 'active'];

    public function subjectprototype() {
        return $this->belongsTo(Subjectprototype::class);
    }

    public function subjectsettings() {
        return $this->hasOne(Subjectsettings::class);
    }

    public function usersettings() {
        return $this->hasOne(Subject_user::class);
    }
}

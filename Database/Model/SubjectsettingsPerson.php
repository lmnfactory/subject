<?php

namespace Lmn\Subject\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Subject\Database\Model\Subjectsettings;
use Lmn\University\Database\Model\Universityperson;

class SubjectsettingsPerson extends Model {

    protected $table = 'subjectsettings_person';

    protected $fillable = ['subjectsettings_id', 'universityperson_id', 'universitypersontype_id'];

    public function person() {
        return $this->belongsTo(Universityperson::class, 'universityperson_id');
    }
}

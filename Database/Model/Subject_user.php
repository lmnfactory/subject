<?php

namespace Lmn\Subject\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Subject_user extends Model {

    protected $table = 'subject_user';

    protected $fillable = ['subject_id', 'user_id', 'lastvisit'];

    public function subject() {
        return $this->belongsTo(Subject::class);
    }
}

<?php

namespace Lmn\Subject\Database\Model;

use Illuminate\Database\Eloquent\Model;

use Lmn\University\Database\Model\Faculty;

class Subjectprototype extends Model {

    protected $table = 'subjectprototype';

    protected $fillable = ['faculty_id', 'version_each', 'version_start', 'public_id', 'name', 'code'];

    public function faculty() {
        return $this->belongsTo(Faculty::class);
    }
}

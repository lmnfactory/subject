<?php

namespace Lmn\Subject\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Subjectrating extends Model {

    protected $table = 'subjectrating';

    protected $fillable = ['subject_id', 'user_id', 'value'];
}

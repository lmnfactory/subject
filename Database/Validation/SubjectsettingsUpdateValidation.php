<?php

namespace Lmn\Subject\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class SubjectsettingsUpdateValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_id' => 'required|exists:subject,id',
            'edited_by' => 'required:exists:user,id',
            'description' => '',
            'webpage' => '',
            'pointslimit' => 'min:0',
            'pointsdelimeter' => 'min:0',
            'universitypersons' => ''
        ];
    }
}

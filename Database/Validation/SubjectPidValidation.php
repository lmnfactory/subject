<?php

namespace Lmn\Subject\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class SubjectPidValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_pid' => 'required|exists:subjectprototype,public_id'
        ];
    }
}

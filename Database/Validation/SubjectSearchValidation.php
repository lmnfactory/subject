<?php

namespace Lmn\Subject\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class SubjectSearchValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'search' => 'required'
        ];
    }
}

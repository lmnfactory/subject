<?php

namespace Lmn\Subject\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class SubjectLeaveValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_id' => 'required|exists:subjectprototype,id'
        ];
    }
}

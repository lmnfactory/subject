<?php

namespace Lmn\Subject\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class SubjectIdValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'id' => 'required|exists:subject,id'
        ];
    }
}

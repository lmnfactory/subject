<?php

namespace Lmn\Subject\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class CreateSubjectFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'faculty_id' => 'required|exists:faculty,id',
            'name' => 'required|max:255',
            'code' => 'required|max:6'
        ];
    }
}

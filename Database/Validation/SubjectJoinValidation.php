<?php

namespace Lmn\Subject\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class SubjectJoinValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_id' => 'required|exists:subjectprototype,id|unique_for:subject_user,subject_id,user_id,user_id'
        ];
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsettingsPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjectsettings_person', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subjectsettings_id')->unsigned();
            $table->integer('universityperson_id')->unsigned();
            $table->integer('universitypersontype_id')->unsigned();
            $table->timestamps();

            $table->index('subjectsettings_id');
            $table->index('universityperson_id');
            $table->index('universitypersontype_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjectsettings_person');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjectsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id')->unsigned();
            $table->integer('edited_by')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->text('webpage')->nullable();
            $table->integer('pointslimit')->nullable();
            $table->integer('pointsdelimeter')->nullable();
            $table->timestamps();

            $table->index('subject_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjectsettings');
    }
}

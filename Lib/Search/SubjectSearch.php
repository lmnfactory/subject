<?php

namespace Lmn\Subject\Lib\Search;

use Lmn\Core\Lib\Search\Searchable;
use Lmn\Core\Lib\Search\SearchFactory;

use Lmn\Subject\Repository\SubjectRepository;

class SubjectSearch implements Searchable {

    private $subjectRepo;
    private $searchFactory;

    public function __construct(SubjectRepository $subjectRepo, SearchFactory $searchFactory) {
        $this->subjectRepo = $subjectRepo;
        $this->searchFactory = $searchFactory;
    }

    public function search($search) {
        $subjectsQuery = $this->subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('subject.search', ['search' => $search]);
            
        $subjects = $subjectsQuery->all();

        $results = [];
        foreach ($subjects as $s) {
            $results[] = $this->searchFactory->result($s, 1, 'subject');
        }

        return $results;
    }
}
<?php

namespace Lmn\Subject\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;
use Lmn\Subject\Repository\SubjectRepository;

class SubjectAutocompleteCache implements Cacheable {

    private $repo;

    public function __construct(SubjectRepository $repo) {
        $this->repo = $repo;
    }

    public function cache() {
        $list = $this->repo->clear()
            ->criteria('subject.default')
            ->criteria('subject.with.all')
            ->criteria('core.limit', ['limit' => 30])
            ->all();

        return $list;
    }
}

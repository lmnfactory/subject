<?php

namespace Lmn\Subject\Lib\Subject;

use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\Subject\Database\Model\Subjectprototype;
use Lmn\Subject\Database\Model\Subject;
use Lmn\Subject\Database\Model\Subjectsettings;

use Lmn\Subject\Repository\SubjectRepository;
use Lmn\Subject\Repository\SubjectUserRepository;

class SubjectUserService {

    private $subjectUserRepo;
    private $subjectRepo;

    public function __construct(SubjectUserRepository $subjectUserRepo, SubjectRepository $subjectRepo) {
        $this->subjectUserRepo = $subjectUserRepo;
        $this->subjectRepo = $subjectRepo;
    }

    public function forUser($userId) {
        return $this->subjectRepo->clear()
            ->criteria('subjectuser.by.user', ['userId' => $userId])
            ->all();
    }

    public function forSubject($subejctId)
    {
        return $this->subjectUserRepo->clear()
            ->criteria('user.by.subject', ['subjectId' => $subejctId])
            ->all();
    }

    public function seen($publicId, $userId, $time = null) {
        if ($time == null) {
            $time = time();
        }

        $subject = $this->subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.by.publicId', ['publicId' => $publicId])
            ->get();
        
        $this->subjectUserRepo->clear()
            ->criteria('subjectuser.unique', ['userId' => $userId, 'subjectId' => $subject->id])
            ->update([
                'lastvisit' => $time
            ]);
    }
}

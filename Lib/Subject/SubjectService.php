<?php

namespace Lmn\Subject\Lib\Subject;

use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\Subject\Database\Model\Subjectprototype;
use Lmn\Subject\Database\Model\Subject;
use Lmn\Subject\Database\Model\Subjectsettings;

class SubjectService {

    private $subjecRepo;

    public function __construct(SubejctRepository $subjectRepo) {
        $this->subjectRepo = $subjecRepo;
    }

    private function genPublicId() {
        $generator = \App::make(GeneratorService::class);
        return $generator->uniqueString('subjectprototype', 'public_id');
    }

    public function getActive($subjectprototypeId) {

    }

    public function save($data) {
        $saveService = \App::make(SaveService::class);

        $subjectprototype = new Subjectprototype();
        $subjectprototype->fill($data);
        $subjectprototype->public_id = $this->genPublicId();

        $subjectsettings = new Subjectsettings();

        $subject = new Subject();
        $subject->active = 1;

        $saveService->begin()
            ->has($subject, $subjectprototype)
            ->has($subjectsettings, $subject)
            ->transaction();

        $subject->subjectprototype;
        $subject->subjectsettings;

        return $subject;
    }

    public function archive($subjectprototypeId) {
        $subject = new Subject();
        $subject->active = 1;
        $subject->subjectprototype_id = $subjectprototypeId;
        \DB::transaction(function() use ($subject){
            Subject::where('subjectprototype_id', '=', $subject->subjectprototype_id)
                ->where('active', '=', 1)
                ->update([
                    'active' => 0
                ]);

            $subject->save();
            $subject->subjectprototype;
        });

        return $subject;
    }

    public function addUser($subject, $user) {
        $data = [
            'user_id' => $user->id,
            'subject_id' => $subject->id
        ];
        return Subject_user::create($data);
    }
}

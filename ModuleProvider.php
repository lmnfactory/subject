<?php

namespace Lmn\Subject;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Lmn\Core\Lib\Database\Seeder\SeederService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Account\Middleware\SigninRequiredMiddleware;

use Lmn\Subject\Lib\Subject\SubjectService;
use Lmn\Subject\Lib\Subject\SubjectUserService;

use Lmn\Subject\Database\Seed\SubjectSeeder;
use Lmn\Subject\Database\Seed\SubjectprototypeSeeder;
use Lmn\Subject\Database\Seed\SubjectsettingsSeeder;
use Lmn\Subject\Database\Seed\SubjectsettingsPersonSeeder;
use Lmn\Subject\Database\Seed\SubjectUserSeeder;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

use Lmn\Subject\Database\Validation\CreateSubjectFormValidation;
use Lmn\Subject\Database\Validation\SubjectPidValidation;
use Lmn\Subject\Database\Validation\SubjectIdValidation;
use Lmn\Subject\Database\Validation\SubjectsettingsUpdateValidation;
use Lmn\Subject\Database\Validation\SubjectJoinValidation;
use Lmn\Subject\Database\Validation\SubjectLeaveValidation;
use Lmn\Subject\Database\Validation\SubjectSearchValidation;

use Lmn\Subject\Repository\SubjectRepository;
use Lmn\Subject\Repository\SubjectUserRepository;
use Lmn\Subject\Repository\SubjectUserForUserRepository;
use Lmn\Subject\Repository\SubjectsettingsPersonRepository;
use Lmn\Subject\Repository\Criteria\SubjectSearchCriteria;
use Lmn\Subject\Repository\Criteria\SubjectDefaultCriteria;
use Lmn\Subject\Repository\Criteria\SubjectByPublicIdCriteria;
use Lmn\Subject\Repository\Criteria\SubjectExtensionIdCriteria;
use Lmn\Subject\Repository\Criteria\SubjectExtensionIdsCriteria;
use Lmn\Subject\Repository\Criteria\SubjectuserExtensionIdCriteria;
use Lmn\Subject\Repository\Criteria\SubjectuserExtensionIdsCriteria;
use Lmn\Subject\Repository\Criteria\SubjectsettingsExtensionIdCriteria;
use Lmn\Subject\Repository\Criteria\UserBySubjectCriteria;
use Lmn\Subject\Repository\Criteria\SubjectByUserCriteria;
use Lmn\Subject\Repository\Criteria\SubjectWithPrototypeCriteria;
use Lmn\Subject\Repository\Criteria\SubjectWithAllCriteria;
use Lmn\Subject\Repository\Criteria\SubjectsettingsWithAllCriteria;
use Lmn\Subject\Repository\Criteria\SubjectUserBySubjectPublicIdCriteria;
use Lmn\Subject\Repository\Criteria\SubjectUserByUserCriteria;
use Lmn\Subject\Repository\Criteria\SubjectUserUniqueCriteria;
use Lmn\Subject\Repository\Criteria\SubjectuserWithSubjectCriteria;
use Lmn\Subject\Repository\Criteria\SubjectWithSettingsCriteria;

use Lmn\Core\Lib\Search\SearchService;
use Lmn\Subject\Lib\Search\SubjectSearch;

use Lmn\Core\Lib\Cache\CacheService;
use Lmn\Subject\Lib\Cache\SubjectAutocompleteCache;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        // Subject service
        $app->singleton(SubjectService::class, function($app) {
            return new SubjectService();
        });
        $app->singleton(SubjectUserService::class, SubjectUserService::class);

        $app->singleton(SubjectRepository::class, function($app) {
            return new SubjectRepository($app->make(CriteriaService::class), $app->make(GeneratorService::class), $app->make(SaveService::class));
        });
        $app->singleton(SubjectUserRepository::class, SubjectUserRepository::class);
        $app->singleton(SubjectUserForUserRepository::class, SubjectUserForUserRepository::class);
        $app->singleton(SubjectsettingsPersonRepository::class, SubjectsettingsPersonRepository::class);

        $app->bind(SubjectSearch::class, SubjectSearch::class);

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        $seederService = \App::make(SeederService::class);
        $seederService->addSeeder(SubjectprototypeSeeder::class);
        $seederService->addSeeder(SubjectSeeder::class);
        $seederService->addSeeder(SubjectsettingsSeeder::class);
        $seederService->addSeeder(SubjectsettingsPersonSeeder::class);
        $seederService->addSeeder(SubjectUserSeeder::class);

        $validationService = \App::make(ValidationService::class);
        $validationService->add('form.subject.create', function() {
            return new CreateSubjectFormValidation();
        });
        $validationService->add('subject.public_id', SubjectPidValidation::class);
        $validationService->add('subject.id', SubjectIdValidation::class);
        $validationService->add('subject.join', SubjectJoinValidation::class);
        $validationService->add('subject.leave', SubjectLeaveValidation::class);
        $validationService->add('subject.search', SubjectSearchValidation::class);
        $validationService->add('form.subjectsettings.update', SubjectsettingsUpdateValidation::class);

        /** @var CriteriaService $criteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('subject.by.publicId', SubjectByPublicIdCriteria::class);
        $criteriaService->add('subject.default', SubjectDefaultCriteria::class);
        $criteriaService->add('subject.search', SubjectSearchCriteria::class);
        $criteriaService->add('subject.ext.id', SubjectExtensionIdCriteria::class);
        $criteriaService->add('subject.ext.ids', SubjectExtensionIdsCriteria::class);
        $criteriaService->add('subjectuser.ext.id', SubjectuserExtensionIdCriteria::class);
        $criteriaService->add('subjectuser.ext.ids', SubjectuserExtensionIdsCriteria::class);
        $criteriaService->add('subjectsettings.ext.id', SubjectsettingsExtensionIdCriteria::class);
        $criteriaService->add('user.by.subject', UserBySubjectCriteria::class);
        $criteriaService->add('subjectuser.by.user', SubjectByUserCriteria::class);
        $criteriaService->add('subject.with.subjectprototype', SubjectWithPrototypeCriteria::class);
        $criteriaService->add('subject.with.all', SubjectWithAllCriteria::class);
        $criteriaService->add('subjectsettings.with.all', SubjectsettingsWithAllCriteria::class);
        $criteriaService->add('subject.with.settings', SubjectWithSettingsCriteria::class);
        $criteriaService->add('subjectuser.unique', SubjectUserUniqueCriteria::class);
        $criteriaService->add('subjectuser.with.subject', SubjectuserWithSubjectCriteria::class);

        $searchService = $app->make(SearchService::class);
        $searchService->add($app->make(SubjectSearch::class));

        $cacheService = \App::make(CacheService::class);
        $cacheService->add('subjectAutocomplete', $app->make(SubjectAutocompleteCache::class));
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Subject\\Controller'], function() {
            Route::post('api/subject/detail','SubjectController@getDetail')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/subject/list','SubjectController@getList');
            Route::post('api/subject/search','SubjectController@search');
            Route::post('api/subject/create','SubjectController@create')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/subject/update','SubjectController@save');
            Route::post('api/subject/prototype','SubjectController@getPrototype')->middleware(SigninRequiredMiddleware::class);

            Route::post('api/subject/settings','SubjectsettingsController@getDetail')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/subject/settings/update','SubjectsettingsController@update')->middleware(SigninRequiredMiddleware::class);

            Route::post('api/user/subjects','SubjectUserController@getSubjects')->middleware(SigninRequiredMiddleware::class);
        });
    }

    public function registerCommands($provider){

    }
}
